FROM adoptopenjdk/openjdk11:latest
LABEL author="youssef brini"
COPY target/Altersis-app-0.0.1-SNAPSHOT.jar .
ENTRYPOINT ["java","-jar","Altersis-app-0.0.1-SNAPSHOT.jar"]
EXPOSE 8080
